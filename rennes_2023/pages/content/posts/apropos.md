---
title: À propos de
slug: À propos de
description: null
draft: false
menu:
  main:
    name: À propos de
    weight: 111
url: /apropos/
---

![Rennes](./imgs/B_Rennes_Inria_d.jpg)
Suite aux premiers rencontres à Rennes et à Bordeaux et à l'occasion de la
[semaine européenne de la e-santé a
Rennes](https://esante.gouv.fr/la-semaine-europeenne-de-la-e-sante), un
troisième séminaire entre les acteurs de la ligne DevTech en Santé Numérique est
proposé du **mardi 26 au jeudi 28 septembre**.

Si vous vous sentez concerné, n'hésitez pas venir partager vos projets dans cet
espace.  
[~~Inscription
obligatoire~~](https://evento.renater.fr/survey/seminaire-sante-numerique-8adynvnl) (fermée).

[Notes de l'organisation](https://notes.inria.fr/I5-nDQ64RE6DQrdCksTurA#)